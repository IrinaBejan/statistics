import randomdotorg
import plotly.plotly as py
import plotly.graph_objs as go

USER_NAME = 'IrinaBM'
KEY = 't5tu6aybdn'
NAME = 'StatisticsTest'
'''
Homework, in writing:
- Using pure random numbers, generate 100 series of 100 coin-flips each.
- For each series, sum the number of heads. You will have 100 sums with values between 0 and 100.
- Put the sums in 10 "buckets": 0-10, 11-20, ... , 91 - 100.
- Use a bar-graph to represent how many sums you have in each bucket.

Bonus 1:
- Repeat the process, with 50 series of 10 coin flips each, and "buckets": 0 - 1, 2 - 3, ... , 9 - 10

Bonus 2:
- Repeat the process with a 6-sided dice. You pick appropriate series and bucket size. '''


def MakeStatisticsPlot(nrSeries, nrCount, listMembers, member, buckets,plotName):
	py.sign_in(USER_NAME, KEY)
	r = randomdotorg.RandomDotOrg(NAME)

	randomGenerated = r.randrange(0, len(listMembers), 1, amount=(nrSeries*nrCount))

	pos = 0
	counter = [0]*(nrCount+1) #counter[i] = how many series have the "height" equal to i
	for series in range(nrSeries):
		count = 0
	
		for nr in range(nrCount):
			picked = randomGenerated[pos]
			pos += 1
			if listMembers[picked] is member:
				count += 1
		counter[count] += 1

	x = []
	y = []
	nrBucket = 0
	for bucket in buckets:
		count = 0
		nrBucket +=1
		
		for pos in range(bucket[0],bucket[1]+1):
			count += counter[pos]

		label = 'Bucket ' + str(bucket[0]) +'-' + str(bucket[1]) + ' ' + str(count)
		x.append(label)
		y.append(count)


	data = [
		go.Bar(
			x=x,
			y=y
		)
	]

	return py.plot(data,plotName)

	

if __name__ == '__main__':

	url1 = MakeStatisticsPlot(100,100,['H','T'],'H',[[0,10],[11,20],[21,30],[31,40],[41,50],[51,60],[61,70],[71,80],[81,90],[91,100]],'Task1')
	url2 = MakeStatisticsPlot(50,10,['H','T'],'H',[[0,1],[2,3],[4,5],[6,7],[8,9],[10,10]],'Bonus1')
	url3 = MakeStatisticsPlot(100,60,[1,2,3,4,5,6],6,[[0,1],[2,5],[6,8],[9,11],[12,14],[15,18],[19,60]],'Bonus2')

	print url1
	print url2
	print url3




